package com.java.validationservice.model;

public class ValidationModel {

    private String user;
    private String password;
    private String confirmpass;
    private int key;
    public String getUser() {
        return user;
    }
    public void setUser(String user) {
        this.user = user;
    }
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getConfirmpass() {
        return confirmpass;
    }
    public void setConfirmpass(String confirmpass) {
        this.confirmpass = confirmpass;
    }
    public int getKey() {
        return key;
    }
    public void setKey(int key) {
        this.key = key;
    }
    @Override
    public String toString() {
        return "ValidationModel [key=" + key + ", confirmpass=" + confirmpass + ", password=" + password
                + ", user=" + user + "]";
    }

    
    
}
