FROM openjdk:11
VOLUME /tmp
EXPOSE 8080
ARG JAR_FILE=target/validation-service-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/validation-service-0.0.1-SNAPSHOT.jar"]